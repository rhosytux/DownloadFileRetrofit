package com.ronggosukowati.downloadfileretrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Streaming;

/**
 * Created by dev on 1/20/18.
 */

public interface RequestInterface {

    @GET("uploads/pdf/FISIKA_KS_X-_1(_12_September_17)FINAL.pdf")
    @Streaming
    Call<ResponseBody> downloadFile();
}
